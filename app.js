const express = require('express')
const app = express()
const port = 2022;

const parser = require('body-parser')
app.use(parser.json())

const mhsRouter = require('./router/mhsRouter')
app.use("/praktekweb",mhsRouter);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })