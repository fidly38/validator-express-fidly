const exp = require("express");
const { check } = require("express-validator")
const router = exp.Router();
const mhsController =require("../controlller/mhsController");

router.get("/getObjMhs",mhsController.getData);
router.post("/post", [
    check('nama').isLength({ min: 5 }),
    check('kelas').isLength({ min: 2 }),
    check('hobby').isLength({ min: 5 })
], mhsController.methodPost);


module.exports=router;