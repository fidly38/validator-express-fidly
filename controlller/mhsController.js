exports.getData=(req,res,next)=>{
    res.status(200).json({
        nrp:"191014009",
        nama:"Muhammad Fidly Alfirdaus",
        Kelas :"3SI-01"
    })

}

const { validationResult } = require('express-validator');

exports.methodPost = (req, res, next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const nama = req.body.nama;
    const kelas = req.body.kelas;
    const hobby = req.body.hobby;
    res.status(200).json({
        message: 'Post created Successfully',
        biodata: { id: new Date().toISOString(), 
                nama: nama, 
                kelas: kelas,
                hobby: hobby
            }
        });
    }
